﻿using System;
using System.Diagnostics;
using System.Net.Http;
using HomeworkOtus_Interfaces.Classes;
using Microsoft.Extensions.Caching.Memory;

namespace HomeworkOtus_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePathForSavingTransactions = @"D:\transactionRepository.txt";
            Trace.Listeners.Add(new ConsoleTraceListener());

            var currencyConverter = new ExchangeRatesApiConverter(new HttpClient(), new MemoryCache(new MemoryCacheOptions()), "a5cf9da55cb835d0a633a7825b3aa8b5");

            var transactionRepository = new ReadFromFileTransactioRepository(filePathForSavingTransactions);
            var transactionParser = new TransactionParser();

            var budgetApp = new BudgetApplication(transactionRepository, transactionParser, currencyConverter);

            //примеры транзакций, ошибка ввода не отлавливается
            // Зачисление 101 RUB Работодатель
            // Трата -10 USD Техника АлиЭкспресс
            // Перевод -1000 RUB Лехе Спс, бро, выручил!

            string input;
            while ( true )
            {
                Console.WriteLine("Введите новую транзакцию или нажмите S для вывода итогов");

                input = Console.ReadLine();

                if ( input != "S" && input != "s" )
                    budgetApp.AddTransaction(input);
                else break;
            }

            budgetApp.OutputTransactions();
            budgetApp.OutputBalanceInCurrency("RUB");
            Console.Read();
        }
    }
}
