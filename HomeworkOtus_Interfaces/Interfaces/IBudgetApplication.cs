﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkOtus_Interfaces.Interfaces
{
    // Интерфейс приложения
    public interface IBudgetApplication
    {
        // Добавление транзакции
        void AddTransaction(string input);

        // Вывод всех транзакции
        void OutputTransactions();

        // Вывод баланцса в заданной валюте
        void OutputBalanceInCurrency(string currencyCode);
    }
}
