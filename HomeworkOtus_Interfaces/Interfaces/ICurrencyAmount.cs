﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkOtus_Interfaces.Interfaces
{
    // Интрефейс описывает сущность (валюту),
    // хранящую поле значения какой-то суммы, и поле с валютой этой суммы.
    public interface ICurrencyAmount
    {
        // USD, EUR, RUB
        string CurrencyCode { get; } 
        // +/- Value
        decimal Amount { get; }
    }
}
