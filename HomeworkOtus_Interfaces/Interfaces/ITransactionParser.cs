﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkOtus_Interfaces.Interfaces
{
    public interface ITransactionParser
    {
        ITransaction Parse(string input);
    }
}
