﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkOtus_Interfaces.Interfaces
{
    // Интрефейс описывает сущность (транзакцию),
    // хранящую поле дату какой-то операции, и поле с суммой по этой операции.
    public interface ITransaction
    {
        DateTimeOffset Date { get; }

        // +/- приход, уход
        ICurrencyAmount Amount { get; }

        string GetTransactionWording();
    }
}
