﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkOtus_Interfaces.Interfaces
{
    public interface ICurrencyConverter
    {
        ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode);
    }
}
