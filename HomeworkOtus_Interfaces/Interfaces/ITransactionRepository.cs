﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkOtus_Interfaces.Interfaces
{
    public interface ITransactionRepository
    {
        void AddTransaction(ITransaction transaction);

        ITransaction[] GetTransactions();
    }
}
