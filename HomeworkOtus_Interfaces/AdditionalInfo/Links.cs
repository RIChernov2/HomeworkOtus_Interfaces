﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkOtus_Interfaces.AdditionalInfo
{
    class Links
    {
        // ссылки по теме
        string s0 = "https://docs.google.com/document/d/1XOdt7pcjVee-prtJ5ufJMA9GxRnxKO9sx4aY12Qf8Fo/edit";

        string s1 = "https://docs.microsoft.com/ru-ru/aspnet/core/performance/caching/memory?view=aspnetcore-5.0" ;
        string s2 = "https://docs.microsoft.com/ru-ru/dotnet/csharp/fundamentals/types/interfaces";
        string s3 = "https://docs.microsoft.com/ru-ru/dotnet/csharp/programming-guide/concepts/covariance-contravariance/";
        string s4 = "https://metanit.com/sharp/tutorial/3.9.php";
        string s5 = "http://sergeyteplyakov.blogspot.com/2011/09/dispose-pattern.html";
        string s6 = "http://sergeyteplyakov.blogspot.com/2014/12/interfaces-vs-abstract-classes.html";
        string s7 = "https://github.com/moq/moq4";
        string s8 = "https://en.wikipedia.org/wiki/Composition_over_inheritance";
        string s9 = "https://github.com/almazsr/OTUS.Interfaces/tree/main/otus-interfaces";
        string s10 = "https://exchangeratesapi.io/";
        string s11 = "https://quicktype.io/";

    }
}
