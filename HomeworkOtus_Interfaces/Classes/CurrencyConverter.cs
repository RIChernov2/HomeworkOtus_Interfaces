﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using HomeworkOtus_Interfaces.Interfaces;

namespace HomeworkOtus_Interfaces.Classes
{
    public class CurrencyConverter : ICurrencyConverter
    {
        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            
            return new CurrencyAmount(currencyCode, amount.Amount);
        }
    }
}
