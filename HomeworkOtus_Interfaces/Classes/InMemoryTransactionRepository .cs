﻿using System;
using System.Collections.Generic;
using System.Text;
using HomeworkOtus_Interfaces.Interfaces;

namespace HomeworkOtus_Interfaces.Classes
{
    internal class InMemoryTransactionRepository : ITransactionRepository
    {
        private readonly List<ITransaction> _transactionsList = new List<ITransaction>();

        public void AddTransaction(ITransaction transaction)
        {
            _transactionsList.Add(transaction);
        }

        public ITransaction[] GetTransactions()
        {
            return _transactionsList.ToArray();
        }
    }
}
