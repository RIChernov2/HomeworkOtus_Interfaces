﻿using System;
using System.Collections.Generic;
using System.Text;
using HomeworkOtus_Interfaces.Interfaces;

namespace HomeworkOtus_Interfaces.Classes
{
    public class BudgetApplication : IBudgetApplication
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly ITransactionParser _transactionParser;
        private readonly ICurrencyConverter _currencyConverter;

        public BudgetApplication(
                                    ITransactionRepository transactionRepository,
                                    ITransactionParser transactionParser,
                                    ICurrencyConverter currencyConverter
                                )
        {
            _transactionRepository = transactionRepository;
            _transactionParser = transactionParser;
            _currencyConverter = currencyConverter;
        }

        public void AddTransaction(string input)
        {
            ITransaction transaction = _transactionParser.Parse(input);
            _transactionRepository.AddTransaction(transaction);
        }

        public void OutputBalanceInCurrency(string currencyCode)
        {
            decimal currentAmmount = 0;
            ITransaction[] transactions = _transactionRepository.GetTransactions();
            foreach ( ITransaction transaction in transactions )
            {
                ICurrencyAmount currency = transaction.Amount;
                if ( currency.CurrencyCode != currencyCode )
                {
                    currency = _currencyConverter.ConvertCurrency(currency, currencyCode);
                }
                currentAmmount += currency.Amount;
            }
            Console.WriteLine($"Ваш баланс: {currentAmmount} {currencyCode}");
        }

        public void OutputTransactions()
        {
            ITransaction[] transactions = _transactionRepository.GetTransactions();
            foreach ( ITransaction transaction in transactions )
            {
                //Console.WriteLine($"Дата транзакции: {transaction.Date}, сумма транзакции: {transaction.Amount}.");
                Console.WriteLine(transaction);
            }
        }
    }
}
