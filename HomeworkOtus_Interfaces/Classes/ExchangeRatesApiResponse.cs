﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Converters;
using System.Globalization;


namespace HomeworkOtus_Interfaces.Classes
{
    public partial class ExchangeRatesApiResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }

        [JsonProperty("base")]
        public string Base { get; set; }

        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }

        [JsonProperty("rates")]
        public Dictionary<string, double> Rates { get; set; }

    }

    public partial class ExchangeRatesApiResponse
    {
        public static ExchangeRatesApiResponse FromJson(string json) 
            => JsonConvert.DeserializeObject<ExchangeRatesApiResponse>(json, Converter.Settings);
    }

    public static class SerializeExtension // extension?
    {
        public static string ToJson(this ExchangeRatesApiResponse self) 
            => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
