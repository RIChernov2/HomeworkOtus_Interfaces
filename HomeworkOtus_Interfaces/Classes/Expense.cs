﻿using System;
using System.Collections.Generic;
using System.Text;
using HomeworkOtus_Interfaces.Interfaces;

namespace HomeworkOtus_Interfaces.Classes
{
    internal class Expense : ITransaction
    {
        public Expense(ICurrencyAmount amount, DateTimeOffset date, string category, string destination)
        {
            Amount = amount;
            Date = date;
            Category = category;
            Destination = destination;
        }

        public ICurrencyAmount Amount { get; }
        public DateTimeOffset Date { get; }

        public string Category { get; }
        public string Destination { get; }

        // не используем просто {Amount}, чтобы было удобнее считать в Parse
        public string GetTransactionWording() => $"Трата {Amount.Amount} {Amount.CurrencyCode} {Category} {Destination}";
        public override string ToString() => $"Трата {Amount} в {Destination} по категории {Category}";
    }
}
