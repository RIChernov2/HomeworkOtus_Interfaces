﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using HomeworkOtus_Interfaces.Interfaces;

namespace HomeworkOtus_Interfaces.Classes
{
    public class CurrencyAmount : ICurrencyAmount, IEquatable<CurrencyAmount>
    {
        public CurrencyAmount(string currencyCode, decimal amount)
        {
            CurrencyCode = currencyCode;
            Amount = amount;
        }

        public string CurrencyCode { get; }
        public decimal Amount { get; private set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as CurrencyAmount);
        }
        public bool Equals(CurrencyAmount amount)
        {
            return amount != null &&
                   CurrencyCode == amount.CurrencyCode &&
                   Amount == amount.Amount;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CurrencyCode, Amount);
        }

        public static CurrencyAmount operator +(CurrencyAmount firstCurrency, ICurrencyAmount secondCurrency)
        {
            if ( firstCurrency.CurrencyCode != secondCurrency.CurrencyCode )
            {
                throw new InvalidOperationException("Currencies should be equal");
            }
            else return new CurrencyAmount(firstCurrency.CurrencyCode, firstCurrency.Amount + secondCurrency.Amount);
        }

        public static CurrencyAmount operator -(CurrencyAmount firstCurrency, ICurrencyAmount secondCurrency)
        {
            if ( firstCurrency.CurrencyCode != secondCurrency.CurrencyCode )
            {
                throw new InvalidOperationException("Currencies should be equal");
            }
            else return new CurrencyAmount(firstCurrency.CurrencyCode, firstCurrency.Amount - secondCurrency.Amount);
        }

        public static bool operator ==(CurrencyAmount left, CurrencyAmount right)
        {
            return EqualityComparer<CurrencyAmount>.Default.Equals(left, right);
        }

        public static bool operator !=(CurrencyAmount left, CurrencyAmount right)
        {
            return !(left == right);
        }

        // для использовании в транзакции
        public override string ToString()
        {
            return $"{Amount:0.00} {CurrencyCode}";
        }
    }
}
