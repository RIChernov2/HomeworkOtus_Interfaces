﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using HomeworkOtus_Interfaces.Interfaces;

namespace HomeworkOtus_Interfaces.Classes
{
    public class ReadFromFileTransactioRepository : ITransactionRepository
    {

        private readonly List<ITransaction> _transactionsList = new List<ITransaction>();
        private readonly string _filePath;

        public ReadFromFileTransactioRepository(string filePath)
        {
            _filePath = filePath;

        }

        public void AddTransaction(ITransaction transaction)
        {
            try
            {
                using ( StreamWriter streamWriter = new StreamWriter(_filePath, true, Encoding.Default) )
                {
                    streamWriter.WriteLine(transaction.GetTransactionWording());
                }
            }
            catch ( Exception exception )
            {
                Console.WriteLine(exception.Message);
            }
        }

        public ITransaction[] GetTransactions()
        {
            _transactionsList.Clear();
            try
            {
                using ( StreamReader streamReader = new StreamReader(_filePath, Encoding.Default) )
                {
                    string line;
                    TransactionParser transactionParser = new TransactionParser();
                    while ( (line = streamReader.ReadLine()) != null )
                    {
                        _transactionsList.Add(transactionParser.Parse(line));
                    }
                }
            }
            catch ( Exception exception )
            {
                Console.WriteLine(exception.Message);
            }

            return _transactionsList.ToArray();
        }
    }
}
