﻿using System;
using System.Collections.Generic;
using System.Text;
using HomeworkOtus_Interfaces.Interfaces;

namespace HomeworkOtus_Interfaces.Classes
{
    internal class TransactionParser : ITransactionParser
    {

        public ITransaction Parse(string input)
        {
            var splits = input.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            var typeCode = splits[0];
            switch ( typeCode )
            {
                case "Трата":
                    return ParseExpense(splits);
                case "Зачисление":
                    return ParseIncome(splits);
                case "Перевод":
                    return ParseTransfer(splits);
                default:
                    throw new NotImplementedException();
            }
        }

        private ITransaction ParseExpense(string[] splits)
        {
            var date = DateTimeOffset.Now;
            var typeCode = splits[0];
            var currencyAmount = new CurrencyAmount(splits[2], decimal.Parse(splits[1]));
            return new Expense(currencyAmount, date, splits[3], splits[4]);
        }

        private ITransaction ParseIncome(string[] splits)
        {
            var date = DateTimeOffset.Now;
            var typeCode = splits[0];
            var currencyAmount = new CurrencyAmount(splits[2], decimal.Parse(splits[1]));
            return new Income(currencyAmount, date, splits[3]);
        }

        private ITransaction ParseTransfer(string[] splits)
        {
            var date = DateTimeOffset.Now;
            var typeCode = splits[0];
            var currencyAmount = new CurrencyAmount(splits[2], decimal.Parse(splits[1]));
            StringBuilder stringBuilder = new StringBuilder();

            string message = splits[4];
            for ( int i = 5 ; i < splits.Length ; i++ )
            {
                message += " " + splits[i];
            }    
            return new Transfer(currencyAmount, date, splits[3], message);
        }
    }
}
