﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Text;
using HomeworkOtus_Interfaces.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HomeworkOtus_Interfaces.Classes
{
    public class ExchangeRatesApiConverter : ICurrencyConverter
    {
        private readonly HttpClient _httpClient;
        private readonly IMemoryCache _memoryCache;
        private readonly string _apiKey;
        private readonly string _cacheKey = "CurrencyRates";

        public ExchangeRatesApiConverter(HttpClient httpClient, IMemoryCache memoryCache, string apiKey)
        {
            _httpClient = httpClient;
            _memoryCache = memoryCache;
            _apiKey = apiKey;
        }

        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            ExchangeRatesApiResponse actualExchangeRates = GetExchangeRatesOrNullAsync().Result;
            if ( actualExchangeRates == null)
            {
                Console.WriteLine("Ошибка при запросе курса валют, конвертация не произошла!");
                return amount;
            }

            var exchangeRate = 
                (decimal)(actualExchangeRates.Rates[currencyCode] / actualExchangeRates.Rates[amount.CurrencyCode]);

            return new CurrencyAmount(currencyCode, amount.Amount * exchangeRate);
        }

        private async Task<ExchangeRatesApiResponse> GetExchangeRatesOrNullAsync()
        {
            ExchangeRatesApiResponse exchangeRates = GetExchangeRatesFromCachOrNull();
            if ( exchangeRates == null) exchangeRates = await GetExchangeRatesFromSiteOrNullAsync();
            return exchangeRates;
        }

        private ExchangeRatesApiResponse GetExchangeRatesFromCachOrNull()
        {
            ExchangeRatesApiResponse result;
            _memoryCache.TryGetValue(_cacheKey, out result);
            return result;
        }

        private async Task<ExchangeRatesApiResponse> GetExchangeRatesFromSiteOrNullAsync()
        {
            HttpResponseMessage response = 
                await _httpClient.GetAsync($"http://api.exchangeratesapi.io/v1/latest?access_key={_apiKey}");
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch
            {
                return null;
            }
            
            string json = await response.Content.ReadAsStringAsync();
            ExchangeRatesApiResponse exchangeRates = ExchangeRatesApiResponse.FromJson(json);

            _memoryCache.Set<ExchangeRatesApiResponse>(_cacheKey, exchangeRates, TimeSpan.FromSeconds(60));

            return exchangeRates;
        }
    }
}
