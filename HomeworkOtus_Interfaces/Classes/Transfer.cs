﻿using System;
using System.Collections.Generic;
using System.Text;
using HomeworkOtus_Interfaces.Interfaces;

namespace HomeworkOtus_Interfaces.Classes
{
    public class Transfer : ITransaction
    {
        public Transfer(ICurrencyAmount amount, DateTimeOffset date, string destination, string message)
        {
            Amount = amount;
            Date = date;
            Destination = destination;
            Message = message;
        }

        public ICurrencyAmount Amount { get; }
        public DateTimeOffset Date { get; }

        public string Destination { get; }
        public string Message { get; }

        // не используем просто {Amount}, чтобы было удобнее считать в Parse
        public string GetTransactionWording() => $"Перевод {Amount.Amount} {Amount.CurrencyCode} {Destination} {Message}";
        public override string ToString() => $"Перевод {Amount.Amount} {Amount.CurrencyCode} с сообщением \"{Message}\"";

    }
}
