﻿using System;
using System.Collections.Generic;
using System.Text;
using HomeworkOtus_Interfaces.Interfaces;

namespace HomeworkOtus_Interfaces.Classes
{
    public class Income : ITransaction
    {
        public Income(ICurrencyAmount amount, DateTimeOffset date, string source)
        {
            Amount = amount;
            Date = date;
            Source = source;
        }

        public ICurrencyAmount Amount { get; }
        public DateTimeOffset Date { get; }
        public string Source { get; }

        // не используем просто {Amount}, чтобы было удобнее считать в Parse
        public string GetTransactionWording() => $"Зачисление {Amount.Amount} {Amount.CurrencyCode} {Source}";
        public override string ToString() => $"Зачисление {Amount} от {Source}";
    }
}
